package blatstvo.quiz.database;

public class TipsDbSchema {
    public static final class TipsTable {
        public static final String Name = "tips";

        public static final class Cols {
            public static final String ID = "_id";
            public static final String tips = "tips";
        }

    }

}
