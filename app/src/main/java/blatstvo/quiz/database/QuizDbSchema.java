package blatstvo.quiz.database;

public class QuizDbSchema {
    public static final class QuizTable {
        public static final String Name = "quiz";

        public static final class Cols {

            public static final String COUNT_QUESTIONS = "questions";//в 1 значении количество отвеченных вопросов (просто), в остальных (2-3) количество правильно отвеченных
            public static final String COUNT_TIPS = "tips";
            public static final String QUIZ = "quiz";//null - 2, во 2 можно хранить количество вопросов в сессии но пока оно 10 и постоянное, 1 значение хранит здесь текущую последовательность вопросов
            public static final String USER = "user";
            public static final String POINTS = "points";

        }

    }

}
