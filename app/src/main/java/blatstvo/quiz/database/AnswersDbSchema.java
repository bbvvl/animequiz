package blatstvo.quiz.database;

public class AnswersDbSchema {
    public static final class AnswersTable {
        public static final String Name = "answers";

        public static final class Cols {
            public static final String ID = "_id";
            public static final String Answer_ = "answer";
            public static final String Answer_0 = "answer0";
            public static final String Answer_1 = "answer1";
            public static final String Answer_2 = "answer2";
            public static final String Answer_3 = "answer3";
            public static final String Answer_4 = "answer4";
            public static final String Answer_5 = "answer5";
            public static final String Answer_6 = "answer6";
            public static final String Answer_7 = "answer7";

        }

    }

}
