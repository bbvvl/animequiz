package blatstvo.quiz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

import blatstvo.quiz.database.QuizDbSchema.QuizTable.Cols;

import static blatstvo.quiz.database.QuizDbSchema.QuizTable.Name;

public class QuizLab {

    private static final String WHERE_CLAUSE_ID = "_id =?";
    private static final String NOT_WHERE_CLAUSE_ID = "_id !=?";
    private static final String[] ID_CS = {"1"}; //current Session

    private static QuizLab sQuizLab;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static QuizLab get(Context context) {
        if (sQuizLab == null) {
            sQuizLab = new QuizLab(context);


        }
        return sQuizLab;
    }

    private QuizLab(Context context) {

        mContext = context.getApplicationContext();
        mDatabase = new QuizBaseHelper(mContext).getWritableDatabase();

    }

    public Quiz getLastResult() {


        Cursor cursor = mDatabase.query(Name, null, null, null, null, null, null);

        if (cursor.getCount() == 0) return new Quiz();
        cursor.moveToLast();
        CursorWrapper cw = new CursorWrapper(cursor);


        Quiz v = new Quiz(
                cw.getInt(cw.getColumnIndex(Cols.COUNT_QUESTIONS)),
                cw.getInt(cw.getColumnIndex(Cols.COUNT_TIPS)),
                cw.getString(cw.getColumnIndex(Cols.QUIZ)),
                cw.getString(cw.getColumnIndex(Cols.USER)),
                cw.getInt(cw.getColumnIndex(Cols.POINTS))
        );

        cursor.close();


        return v;
    }

    public Quiz getCurrentSession() {


        Cursor cursor = mDatabase.query(Name, null, WHERE_CLAUSE_ID, ID_CS, null, null, null);

        if (cursor.getCount() == 0) return new Quiz();
        System.out.println("WTF");
        cursor.moveToFirst();
        CursorWrapper cw = new CursorWrapper(cursor);


        Quiz v = new Quiz(
                cw.getInt(cw.getColumnIndex(Cols.COUNT_QUESTIONS)),
                cw.getInt(cw.getColumnIndex(Cols.COUNT_TIPS)),
                cw.getString(cw.getColumnIndex(Cols.QUIZ)),
                cw.getString(cw.getColumnIndex(Cols.USER)),
                cw.getInt(cw.getColumnIndex(Cols.POINTS))
        );

        cursor.close();


        return v;
    }

    public void updateCurrentSessionTips(SparseArray<String> tips) {

        mDatabase.delete(TipsDbSchema.TipsTable.Name, null, null);

        if (tips == null) return;

        for (int i = 0, q = 0; i < tips.size(); i++, q++) {

            if (tips.indexOfKey(q) < 0) {
                while (tips.indexOfKey(q) < 0) q++;
            }
            ContentValues values = new ContentValues();
            values.put(TipsDbSchema.TipsTable.Cols.tips, tips.get(q));
            values.put(TipsDbSchema.TipsTable.Cols.ID, q);
            mDatabase.insert(TipsDbSchema.TipsTable.Name, null, values);
        }
    }

    public SparseArray<String> getCurrentSessionTips() {

        SparseArray<String> value = new SparseArray<>();
        Cursor cursor = mDatabase.query(TipsDbSchema.TipsTable.Name, null, null, null, null, null, null);

        if (cursor.getCount() == 0) return null;

        cursor.moveToFirst();
        CursorWrapper cw = new CursorWrapper(cursor);

        for (int i = 0; i < cursor.getCount(); i++) {
            value.put(cw.getInt(cw.getColumnIndex(TipsDbSchema.TipsTable.Cols.ID)), cw.getString(cw.getColumnIndex(TipsDbSchema.TipsTable.Cols.tips)));
            cursor.moveToNext();
        }

        cursor.close();
        return value;

    }

    private void saveCurrentSessionListAnswers(SparseArray<List<String>> listSparseArray) {

        mDatabase.delete(AnswersDbSchema.AnswersTable.Name, null, null);

        if (listSparseArray == null) {
            return;
        }

        for (int i = 0, q = 0; i < listSparseArray.size(); i++, q++) {

            if (listSparseArray.indexOfKey(q) < 0) {
                while (listSparseArray.indexOfKey(q) < 0) q++;
            }
            ContentValues values = getContentValuesForAnswers(listSparseArray.get(q), q);
            mDatabase.insert(AnswersDbSchema.AnswersTable.Name, null, values);
        }


    }

    public SparseArray<List<String>> getCurrentSessionListAnswers() {

        SparseArray<List<String>> value = new SparseArray<>();
        Cursor cursor = mDatabase.query(AnswersDbSchema.AnswersTable.Name, null, null, null, null, null, null);

        if (cursor.getCount() == 0) return null;

        cursor.moveToFirst();
        CursorWrapper cw = new CursorWrapper(cursor);

        for (int i = 0; i < cursor.getCount(); i++) {

            List<String> v1 = new ArrayList<>();
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_0)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_1)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_2)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_3)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_4)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_5)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_6)));
            v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_7)));

            value.put(cw.getInt(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.ID)), v1);
            cursor.moveToNext();
        }

        cursor.close();

        return value;
    }

    public void updateCurrentSession(Quiz c, SparseArray<List<String>> listSparseArray) {
        ContentValues values = getContentValues(c);
        mDatabase.update(Name, values, WHERE_CLAUSE_ID, ID_CS);

        saveCurrentSessionListAnswers(listSparseArray);

    }

    public boolean checkOnExistingCurrentSession() {

        Cursor cursor = mDatabase.query(Name, new String[]{Cols.QUIZ}, WHERE_CLAUSE_ID, ID_CS, null, null, null);

        try {
            if (cursor.getCount() == 0) return false;
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(Cols.QUIZ)) != null;
        } finally {
            cursor.close();
        }

    }

    public void updateAllResults(Quiz c) {

        ContentValues values = getContentValues(c);
        mDatabase.insert(Name, null, values);

    }

    public List<Quiz> getAllResults() {

        Cursor cursor = mDatabase.query(Name, null, NOT_WHERE_CLAUSE_ID, ID_CS, null, null, Cols.POINTS + " DESC", null//(8) + ""
        );

        if (cursor.getCount() == 0) return new ArrayList<>();
        cursor.moveToFirst();
        CursorWrapper cw = new CursorWrapper(cursor);

        List<Quiz> v = new ArrayList<>();

        while (true) {
            v.add(new Quiz(
                            cw.getInt(cw.getColumnIndex(Cols.COUNT_QUESTIONS)),
                            cw.getInt(cw.getColumnIndex(Cols.COUNT_TIPS)),
                            cw.getString(cw.getColumnIndex(Cols.QUIZ)),
                            cw.getString(cw.getColumnIndex(Cols.USER)),
                            cw.getInt(cw.getColumnIndex(Cols.POINTS))
                    )
            );

            if (cursor.isLast()) break;
            cursor.moveToNext();
        }


        cursor.close();

        return v;
    }

    static ContentValues getContentValues(Quiz quiz) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Cols.COUNT_QUESTIONS, quiz.getQuestions());
        contentValues.put(Cols.COUNT_TIPS, quiz.getTips());
        contentValues.put(Cols.QUIZ, quiz.getQuiz());
        contentValues.put(Cols.USER, quiz.getUser());
        contentValues.put(Cols.POINTS, quiz.getPoints());
        return contentValues;
    }

    private static ContentValues getContentValuesForAnswers(List<String> list, int id) {
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            contentValues.put(AnswersDbSchema.AnswersTable.Cols.Answer_ + i, list.get(i));
        }
        contentValues.put(AnswersDbSchema.AnswersTable.Cols.ID, id);
        return contentValues;
    }

}
