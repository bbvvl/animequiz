package blatstvo.quiz.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import blatstvo.quiz.database.QuizDbSchema.QuizTable;

import static blatstvo.quiz.database.QuizDbSchema.QuizTable.Name;
import static blatstvo.quiz.database.QuizLab.getContentValues;


public class QuizBaseHelper extends SQLiteOpenHelper {

    private static final int Version = 1;
    private static final String Database_Name = "quiz.db";

    public QuizBaseHelper(Context context) {
        super(context, Database_Name, null, Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + QuizTable.Name
                + "(  _id integer primary key , "
                + QuizTable.Cols.COUNT_QUESTIONS + ","
                + QuizTable.Cols.COUNT_TIPS + ","
                + QuizTable.Cols.QUIZ + ","
                + QuizTable.Cols.USER + ","
                + QuizTable.Cols.POINTS + ")");

        db.insert(Name, null, getContentValues(new Quiz(0, 0, null, "",-100000)));

        db.execSQL("create table " + AnswersDbSchema.AnswersTable.Name
                + "(  _id integer primary key , "
                + AnswersDbSchema.AnswersTable.Cols.Answer_0 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_1 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_2 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_3 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_4 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_5 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_6 + ","
                + AnswersDbSchema.AnswersTable.Cols.Answer_7 + ")");


        db.execSQL("create table " + TipsDbSchema.TipsTable.Name
                + "(  _id integer primary key , "
                + TipsDbSchema.TipsTable.Cols.tips + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QuizTable.Name);
        db.execSQL("DROP TABLE IF EXISTS " + AnswersDbSchema.AnswersTable.Name);
        db.execSQL("DROP TABLE IF EXISTS " + TipsDbSchema.TipsTable.Name);
        onCreate(db);

    }
}
