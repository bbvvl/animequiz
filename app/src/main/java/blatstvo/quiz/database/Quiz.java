package blatstvo.quiz.database;


public class Quiz {

    private int questions;
    private int tips;
    private int points;
    private String user;
    private String quiz;


    public Quiz(int questions, int tips, String quiz, String user, int points) {

        this.questions = questions;
        this.tips = tips;
        this.quiz = quiz;
        this.user = user;
        this.points = points;
    }


    public Quiz() {

        this.questions = 0;
        this.tips = 0;
        this.quiz = null;
        this.user = "";
        this.points = 0;
    }

    public int getQuestions() {
        return questions;
    }

    public void setQuestions(int questions) {
        this.questions = questions;
    }

    public int getTips() {
        return tips;
    }

    public void setTips(int tips) {
        this.tips = tips;
    }

    public String getQuiz() {
        return quiz;
    }

    public void setQuiz(String quiz) {
        this.quiz = quiz;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }



}

