package blatstvo.quiz.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import blatstvo.quiz.R;
import blatstvo.quiz.database.Quiz;
import blatstvo.quiz.database.QuizLab;

import static blatstvo.quiz.activity.QuizActivity.PAGE_COUNT;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";
    private static final String COUNT_KEY = "seekbar_preference_key";
    public static final String COUNT_ANSWERS = "count_answers";

    public static final String CURRENT_USER = "count_users";

    private static final int SETTINGS_ACTIVITY_REQUEST_CODE = 0;
    private static final int QUIZ_ACTIVITY_REQUEST_CODE = 1;

    private QuizLab mQuizLab;

    private int mCount = 4;

    private EditText mEditText;

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText = (EditText) findViewById(R.id.nicknameEditText);

        mCount = PreferenceManager.getDefaultSharedPreferences(this).getInt(COUNT_KEY, 4);
        // set default values in the app's SharedPreferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        mQuizLab = QuizLab.get(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCount = savedInstanceState.getInt(COUNT_ANSWERS);
    }

    protected void onResume() {
        super.onResume();
        mCount = PreferenceManager.getDefaultSharedPreferences(this).getInt(COUNT_KEY, mCount);
        System.out.println(mQuizLab.checkOnExistingCurrentSession());
        findViewById(R.id.continueGameButton).setEnabled(mQuizLab.checkOnExistingCurrentSession());
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(COUNT_ANSWERS, mCount);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.activity_main_menu_settings:

                showSettings();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onclick(View v) {

        switch (v.getId()) {
            case R.id.newGameButton:

                if (findViewById(R.id.continueGameButton).isEnabled()) {
                    final AlertDialog.Builder builder =
                            new AlertDialog.Builder(this);

                    builder.setMessage(R.string.current_session);

                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    QuizLab.get(getApplicationContext()).updateCurrentSession(new Quiz(), null);
                                    Intent intent2 = new Intent(getApplication(), QuizActivity.class);
                                    intent2.putExtra(CURRENT_USER, mEditText.getText().toString());
                                    intent2.putExtra(COUNT_ANSWERS, mCount);
                                    startActivityForResult(intent2, QUIZ_ACTIVITY_REQUEST_CODE);
                                }
                            }
                    );

                    builder.setNegativeButton(android.R.string.cancel, null);
                    builder.create().show();
                    break;
                }

            case R.id.continueGameButton:

                Intent intent2 = new Intent(this, QuizActivity.class);
                intent2.putExtra(COUNT_ANSWERS, mCount);
                intent2.putExtra(CURRENT_USER, mEditText.getText().toString());
                startActivityForResult(intent2, QUIZ_ACTIVITY_REQUEST_CODE);

                break;
            case R.id.statisticButton:
                showAllResults();
                break;

            case R.id.settingsButton:
                showSettings();
                break;
        }
    }

    private void showSettings() {

        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, SETTINGS_ACTIVITY_REQUEST_CODE);

    }

    private void onResults() {

        Quiz quiz = mQuizLab.getLastResult();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);

        View layout = getLayoutInflater().inflate(R.layout.dialog_view, null);

        builder.setView(layout);
        final AlertDialog ad = builder.create();

        builder.setCancelable(false);
        ((TextView) layout.findViewById(R.id.cu)).setText(quiz.getUser());
        //НЕ трогай слышишь НЕ ТРОГАЙ
        ((TextView) layout.findViewById(R.id.ca)).setText(quiz.getQuestions() + "/" + PAGE_COUNT);
        //quiz.getQuiz());
        ((TextView) layout.findViewById(R.id.ut)).setText(quiz.getTips() + "");
        ((TextView) layout.findViewById(R.id.pt)).setText(quiz.getPoints() + "");
        layout.findViewById(R.id.pb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

        ad.show();
        mQuizLab.updateCurrentSession(new Quiz(), null);
    }

    private void showAllResults() {

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(this);

        builder.setCancelable(false);

        View layout = getLayoutInflater().inflate(R.layout.dialog_view_all, null);
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mRecyclerView = (RecyclerView) layout.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new MyAdapter(mQuizLab.getAllResults()));

        builder.setView(layout);
        final AlertDialog ad = builder.create();
        //ad.setView(layout);
        layout.findViewById(R.id.pb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

        ad.show();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (mRecyclerView != null) {

            Point p = new Point();
            getWindowManager().getDefaultDisplay().getSize(p);

            TypedValue tv = new TypedValue();
            getResources().getValue(R.dimen.contance_to_dialog_view_all_auto_size, tv, true);

            if (mRecyclerView.getHeight() > (tv.getFloat() * p.y))
                mRecyclerView.setLayoutParams(new ConstraintLayout.LayoutParams(mRecyclerView.getWidth(), getHeight(p.y, tv.getFloat())));
        }
    }


    private int getHeight(int y, float tf) {

        int factor = 70;
        //System.out.println("getHeight called "+y+" "+tf);
        int q = (int) ((tf * y) % factor);
        //System.out.println("q "+q);
        q = (q >= (factor/2)) ? (int) (tf * y / factor + 1) : (int) (tf * y / factor);
        //System.out.println("q "+q);
        return q * factor;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case SETTINGS_ACTIVITY_REQUEST_CODE:

                if (mCount != PreferenceManager.getDefaultSharedPreferences(this).getInt(COUNT_KEY, 0)) {
                    mCount = PreferenceManager.getDefaultSharedPreferences(this).getInt(COUNT_KEY, 4);

                    System.out.println("mCount " + mCount);
                    mQuizLab.updateCurrentSession(new Quiz(), null);
                }
                break;

            case QUIZ_ACTIVITY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    onResults();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<Quiz> mListQuiz;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case

            private TextView mPoints;
            private TextView mName;
            private TextView mPosition;

            public ViewHolder(View v) {
                super(v);

                mPoints = (TextView) v.findViewById(R.id.tv_points);
                mPosition = (TextView) v.findViewById(R.id.tv_position);
                mName = (TextView) v.findViewById(R.id.tv_name);
            }

            public void bind(int position) {

                mPoints.setText(mListQuiz.get(position).getPoints() + "");
                mName.setText(mListQuiz.get(position).getUser());
                mPosition.setText((position + 1) + "");
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<Quiz> listQuiz) {
            mListQuiz = listQuiz;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            View view = getLayoutInflater().inflate(R.layout.dialog_view_item, parent, false);
            return new ViewHolder(view);
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.bind(position);

        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mListQuiz.size();
        }
    }
}
