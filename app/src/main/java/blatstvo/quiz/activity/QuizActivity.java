package blatstvo.quiz.activity;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;

import blatstvo.quiz.R;
import blatstvo.quiz.database.Quiz;
import blatstvo.quiz.database.QuizLab;
import blatstvo.quiz.fragment.PageFragment;

import static blatstvo.quiz.activity.MainActivity.COUNT_ANSWERS;
import static blatstvo.quiz.activity.MainActivity.CURRENT_USER;

public class QuizActivity extends AppCompatActivity implements PageFragment.Callback {


    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PICK_CONTACT = 2;
    private static final int CALL_TO_CONTACT = 3;
    public static final int PAGE_COUNT = 10;
    private static final int NUMBER_QUESTIONS = 40;
    private static final String VIEWPAGER_KEY = "viewpager_key";

    private ViewPager mViewPager;
    private TabLayout mLayout;

    private QuizLab mQuizLab;

    //сопоставление массива и позиций
    private List<Integer> mList;
    //отвеченные вопросы
    private List<Integer> mCurrentSession;
    private SparseArray<List<String>> mListWithAnswers;
    private SparseArray<String> mCurrentTips;
    //массив аниме
    String[] mFullList;

    //количество правильных ответов
    private int mCountOfCorrectAnswers = 0;
    //количество использованных подсказок
    private int mCountOfTips = 0;
    private String mUser;

    MenuItem mItemCall, mItemPercent;
    int tabpos = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("onCreate");
        setContentView(R.layout.activity_quiz);
        mUser = getIntent().getStringExtra(CURRENT_USER);
        mQuizLab = QuizLab.get(this);
        mFullList = getResources().getStringArray(R.array.anime_list);
        mCurrentSession = new ArrayList<>();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("onPause");
        StringBuilder sb = new StringBuilder();

        System.out.println("sb.append(mList).append(mCurrentSession).toString() " + sb.append(mList).append(mCurrentSession).toString());
        mQuizLab.updateCurrentSession
                (new Quiz(mCountOfCorrectAnswers, mCountOfTips, sb.append(mList).append(mCurrentSession).toString(), mUser,
                                getPoints()
                        )
                        , mListWithAnswers
                );

        System.out.println("mQuizLab " + mQuizLab.checkOnExistingCurrentSession());
        mQuizLab.updateCurrentSessionTips(mCurrentTips);
        tabpos = mViewPager.getCurrentItem();
    }


    @Override
    public void onResume() {
        super.onResume();
        System.out.println("onResume");

        if (mUser == null) mUser = mQuizLab.getCurrentSession().getUser();

        if (mQuizLab.checkOnExistingCurrentSession()) {
            Quiz value = mQuizLab.getCurrentSession();

            Integer[] list = new Integer[PAGE_COUNT];
            String toList = parseStringF(value.getQuiz(), '[', ']');
            for (int i = 0; i < PAGE_COUNT; i++) {
                if (i == PAGE_COUNT - 1) {
                    list[i] = Integer.parseInt(toList);
                } else {
                    list[i] = Integer.parseInt(parseStringF(toList, '\0', ','));
                }
                toList = toList.substring(toList.indexOf('\u0020') + 1);


            }

            mList = Arrays.asList(list);
            toList = parseStringL(value.getQuiz(), '[', ']');

            if (toList.length() != 0) {
                mCurrentSession = new ArrayList<>();

                while (true) {

                    System.out.println(toList + " ]" + mCurrentSession.size());
                    if (toList.length() > 2)
                        mCurrentSession.add(Integer.parseInt(parseStringF(toList, '\0', ',')));
                    else {
                        mCurrentSession.add(Integer.parseInt(toList));

                        break;
                    }
                    toList = toList.substring(toList.indexOf('\u0020') + 1);
                }
            }
            mCountOfCorrectAnswers = value.getQuestions();
            mListWithAnswers = mQuizLab.getCurrentSessionListAnswers();
            mCountOfTips = value.getTips();

            mCurrentTips = mQuizLab.getCurrentSessionTips();
            if (mCurrentTips == null) mCurrentTips = new SparseArray<>();

            while (mCurrentSession.contains(tabpos)) {
                tabpos++;
                if (tabpos == PAGE_COUNT) tabpos = 0;
            }

        } else {

            System.out.println("THIS IS NULL");
            mCountOfTips = 0;
            mCountOfCorrectAnswers = 0;
            mList = randomList();
            mListWithAnswers = new SparseArray<>();
            mCurrentTips = new SparseArray<>();
        }


        initViewPager();
        mViewPager.setCurrentItem(tabpos);

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("onStart");

    }

    public String parseStringF(String s, char v, char z) {
        return s.substring(s.indexOf(v) + 1, s.indexOf(z));
    }

    public String parseStringL(String s, char v, char z) {
        return s.substring(s.lastIndexOf(v) + 1, s.lastIndexOf(z));
    }

    private void initViewPager() {

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), getIntent().getIntExtra(COUNT_ANSWERS, 4)));


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private float sumPositionAndPositionOffset;
            boolean isSwipeToLeft;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                isSwipeToLeft = position + positionOffset > sumPositionAndPositionOffset;
                sumPositionAndPositionOffset = position + positionOffset;

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("VP onPageSelected");
                if (mCurrentSession.contains(position)) {

                    while (mCurrentSession.contains(position)) {
                        if (!isSwipeToLeft) {
                            --position;
                        } else {
                            ++position;
                        }
                        if (position == -1) position = PAGE_COUNT - 1;
                        if (position == PAGE_COUNT) position = 0;

                    }

                    tabpos = position;
                }

                if (mItemPercent != null) {
                    if (!(mCurrentTips.indexOfKey(position) < 0)) {

                        mItemPercent.setEnabled(false);
                    } else mItemPercent.setEnabled(true);
                }
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mLayout = (TabLayout)

                findViewById(R.id.sliding_tabs);

        mLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()

        {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (mCurrentSession.contains(tab.getPosition())) {

                    /*if (tab.getPosition() == 0) mViewPager.setCurrentItem(PAGE_COUNT - 1);
                    else*/
                    mViewPager.setCurrentItem(tabpos);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mLayout.setupWithViewPager(mViewPager);


        for (
                int i = 0;
                i < PAGE_COUNT; i++)

        {

            mLayout.getTabAt(i).setCustomView(R.layout.tab_view);

            ((TextView) mLayout.getTabAt(i).getCustomView()
                    .findViewById(R.id.tab_view_textview)).setText(mViewPager.getAdapter().getPageTitle(i));
        }

    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        System.out.println("onSaveInstanceState");
        outState.putInt(VIEWPAGER_KEY, tabpos);

    }


    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        System.out.println("onRestoreInstanceState");
        tabpos = savedInstanceState.getInt(VIEWPAGER_KEY);
    }


    @Override
    public void onClick(int position, View v) {

        Drawable d = v.getBackground();
        if (((Button) v).getText().equals(mFullList[mList.get(position)])) {
            mCountOfCorrectAnswers++;
            d.setColorFilter(getResources().getColor(R.color.correct), PorterDuff.Mode.MULTIPLY);
        } else {
            d.setColorFilter(getResources().getColor(R.color.incorrect), PorterDuff.Mode.MULTIPLY);
        }

        mItemPercent.setEnabled(false);
        if (!mCurrentSession.contains(position))

        {
            System.out.println("pos " + position + " size  " + mCurrentSession.size());
            mCurrentSession.add(position);
        }


        System.out.println("mCurrentSession.size() " + mCurrentSession.size());
        if (mCurrentSession.size() == PAGE_COUNT) {

            mQuizLab.updateAllResults(new Quiz(mCountOfCorrectAnswers, mCountOfTips, null,
                    (mUser.length() != 0) ? mUser : "nameless"
                    , getPoints()));

            setResult(Activity.RESULT_OK);
            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        System.out.println("onCreateOptionsMenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_quiz_menu, menu);


        mItemCall = menu.findItem(R.id.activity_quiz_menu_call);

        mItemPercent = menu.findItem(R.id.activity_quiz_menu_percent);


        if (mCurrentTips == null) {
            mItemCall.setEnabled(mCountOfTips == 0);
            mCurrentTips = new SparseArray<>();
        } else {

            mItemCall.setEnabled((mCountOfTips - mCurrentTips.size()) == 0);
            mItemPercent.setEnabled(mCurrentTips.indexOfKey(mViewPager.getCurrentItem()) < 0);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("onOptionsItemSelected");
        switch (item.getItemId()) {

            case R.id.activity_quiz_menu_call:

                checkAndRequestPermissions();
                return true;

            case R.id.activity_quiz_menu_percent:
                List<Integer> list = new ArrayList<>();
                int ca = getIntent().getIntExtra(COUNT_ANSWERS, 4);
                Random random = new Random();
                for (int i = 0; i < (ca / 2) + 1; i++) {

                    int value = random.nextInt(ca) + 1;
                    if (!list.contains(value))
                        list.add(value);
                    else i--;
                }
                ((PageFragment) ((MyFragmentPagerAdapter) mViewPager.getAdapter())
                        .getRegisteredFragment(mViewPager.getCurrentItem()))
                        .setHalfCountAnswers(list, mFullList[mList.get(mViewPager.getCurrentItem())]);


                mCurrentTips.put(mViewPager.getCurrentItem(), list.toString());

                mCountOfTips++;
                mItemPercent.setEnabled(false);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void checkAndRequestPermissions() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {


            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE},
                        PERMISSIONS_REQUEST_READ_CONTACTS);
            }


        } else {
            System.out.println("readContactAndCall in check");
            readContactAndCall();
        }
        //checkOnExistingCurrentSession();
    }

    public void readContactAndCall() {

        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                {
                    System.out.println("readContactAndCall in onRequestPermissionsResult");
                    readContactAndCall();
                }

            } else {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CALL_PHONE)) {
                    final AlertDialog.Builder builder =
                            new AlertDialog.Builder(this);

                    builder.setMessage(R.string.permission);

                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (Build.VERSION.SDK_INT >= 23)
                                        requestPermissions(new String[]{
                                                        Manifest.permission.READ_CONTACTS,
                                                        Manifest.permission.CALL_PHONE},
                                                PERMISSIONS_REQUEST_READ_CONTACTS);
                                }
                            }
                    );

                    builder.setNegativeButton(android.R.string.cancel, null);
                    builder.create().show();

                } else showNoPermissionSnackbar();
            }
        } else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void showNoPermissionSnackbar() {
        Snackbar.make(findViewById(R.id.ConstraintLayout), getResources().getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                .setAction("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.snackbar_settings_text),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                })
                .show();
    }

    private void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                checkAndRequestPermissions();//readContactAndCall();
                break;
            case PICK_CONTACT:
                if (resultCode == Activity.RESULT_OK) {

                    String name = "";

                    Cursor c = getContentResolver().query(data.getData(), null, null, null, null);

                    if (c.moveToFirst()) {
                        name = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                    }
                    c.close();
                    Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{name},
                            null);

                    try {
                        if (cursorPhone != null && cursorPhone.moveToFirst()) {
                            name = cursorPhone.getString(0);
                        }
                    } finally {
                        cursorPhone.close();
                    }
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + name));

                    //i can`t skip this :(

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }


                    startActivityForResult(intent, CALL_TO_CONTACT);
                    break;
                }

            case CALL_TO_CONTACT:
                mCountOfTips++;
                onPause();
                mItemCall.setEnabled(false);

                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private static List<Integer> randomList() {

        List<Integer> value = new ArrayList<>();

        Random random = new Random();

        LinkedHashSet<Integer> q = new LinkedHashSet<>();
        while (q.size() < PAGE_COUNT) {
            q.add(random.nextInt(NUMBER_QUESTIONS));
        }
        value.addAll(q);
        return value;
    }


    private List<String> randomListWithAnswers(String[] q, int position, int count) {
        List<String> value = new ArrayList<>();

        value.add(q[position]);
        Random random = new Random();
        while (value.size() != count) {

            int i = random.nextInt(NUMBER_QUESTIONS);
            if (!value.contains(q[i])) value.add(q[i]);
        }

        Collections.shuffle(value);
        return value;
    }

    private int getPoints() {
        return (mCountOfCorrectAnswers * PAGE_COUNT - (mCountOfTips * PAGE_COUNT) / 2) * getIntent().getIntExtra(COUNT_ANSWERS, 4);
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        int mCount;

        MyFragmentPagerAdapter(FragmentManager fm, int countAnswers) {
            super(fm);

            mCount = countAnswers;
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("getItem");
            if (mListWithAnswers.get(position) == null)
                mListWithAnswers.append(position, randomListWithAnswers(mFullList, mList.get(position), mCount));

            return PageFragment.newInstance(mList.get(position), (ArrayList<String>) mListWithAnswers.get(position), position, mCount);


        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Quest " + (position + 1);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            System.out.println("instantiateItem " + position);
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            System.out.println("destroyItem " + position);
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public void finishUpdate(ViewGroup contain) {
            super.finishUpdate(contain);


            System.out.println("finishUpdate " + mViewPager.getCurrentItem());

            List<Integer> list = new ArrayList<>();
            if (mViewPager.getCurrentItem() > 0)
                list.add((mViewPager.getCurrentItem() - 1));
            list.add(mViewPager.getCurrentItem());

            if (mViewPager.getCurrentItem() < (PAGE_COUNT - 1))
                list.add(mViewPager.getCurrentItem() + 1);

            for (int i : list) {
                if (registeredFragments.get(i) == null) continue;

                if (!(mCurrentTips.indexOfKey(i) < 0)) {

                    String s = mCurrentTips.get(i);
                    s = s.substring(s.indexOf('[') + 1, s.indexOf(']'));

                    List<Integer> values = new ArrayList<>();
                    while (true) {

                        if (s.length() == 1) {
                            values.add(Integer.parseInt(s));
                            break;
                        } else {

                            values.add(Integer.parseInt(s.substring(0, s.indexOf(','))));
                        }
                        s = s.substring(s.indexOf(' ') + 1);

                    }
                    ((PageFragment) registeredFragments.get(i))
                            .setHalfCountAnswers(values, mFullList[mList.get(i)]);

                }
            }
        }

        Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

    }

}