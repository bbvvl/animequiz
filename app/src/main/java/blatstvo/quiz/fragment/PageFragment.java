package blatstvo.quiz.fragment;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import blatstvo.quiz.R;
import blatstvo.quiz.activity.QuizActivity;

import static android.content.ContentValues.TAG;


public class PageFragment extends Fragment {


    private static final String DRAWABLE = "drawable";
    private static final String ANSWERS = "answers";
    private static final String POSTION = "position";
    private static final String COUNT_ANSWERS = "count_answers";

    private int mCountAnswers;
    private int pageNumber;

    private Callback mCallback;


    public static PageFragment newInstance(int drawable, ArrayList<String> answers, int position, int countAnswers) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(DRAWABLE, drawable);
        arguments.putInt(POSTION, position);
        arguments.putInt(COUNT_ANSWERS, countAnswers);
        arguments.putStringArrayList(ANSWERS, answers);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageNumber = getArguments().getInt(POSTION);
        mCountAnswers = getArguments().getInt(COUNT_ANSWERS);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment, null);
        ImageView v = (ImageView) view.findViewById(R.id.view);

        try {
            InputStream stream =
                    getActivity().getAssets().open("drawable" + "/" + (getArguments().getInt(DRAWABLE) + 1) + ".jpg");
            // load the asset as a Drawable and display on the flagImageView
            Drawable flag = Drawable.createFromStream(stream, null);
            v.setImageDrawable(flag);
        } catch (IOException exception) {
            Log.e(TAG, "Error loading  drawable" + "/" + (getArguments().getInt(DRAWABLE) + 1) + ".jpg ", exception);
        }

        setCountAnswers(view, mCountAnswers);
        setAnswersAndButtonClickListener(view);

        return view;
    }


    public interface Callback {

        public void onClick(int pageNumber, View v);
    }

    private void setAnswersAndButtonClickListener(View v) {
        ArrayList<String> answers = getArguments().getStringArrayList(ANSWERS);
//        Collections.shuffle(answers);

        for (int i = 1; i <= mCountAnswers; i++) {
            View view = v.findViewById(getResources().getIdentifier("btn" + i, "id", getActivity().getPackageName()));

            ((Button) view).setText(answers.get(i - 1));
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v1) {

                    setNotClickable();
                    mCallback.onClick(pageNumber, v1);
                }
            });
        }
    }

    private void setNotClickable() {

        for (int i = 1; i <= mCountAnswers; i++) {
            View view = getView().findViewById(getResources().getIdentifier("btn" + i, "id", getActivity().getPackageName()));
            view.setClickable(false);

        }
    }

    //МЕТОД УСТАНОВкИ кОЛИЧЕСТВА ВАРИАНТОВ ОТВЕТА
    //ИЗВЛЕкАЕМ ЧИСЛО ЕСЛИ ОНО ДЕЛИТСЯ НА 2 ТО В ЦИкЛЕ УСТАНАВЛИВАЕМ ВАЙТ = 0 ДЛЯ 8-ОЛИЧЕСТВО П кНОПОк НАЧИНАЯ С кОНЦА
    // НО ЧИСЛО 8-кОЛИЧЕСТВО ДЕЛИМ НА 2 БЕЗ ОСТАТкА -> УСТАНАВЛИВАЕМ ИМ ВАЙТ 0 И СЛЕДУЮЩЕЙ кНОПкЕ СТАВИМ ИНВИЗИБЛ (+ДИЗАБЛ ЕСЛИ НАДО).
    private void setCountAnswers(View v, int countAnswers) {

        ConstraintLayout root = (ConstraintLayout) v.findViewById(R.id.fragment_answers_constraint_layout);

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(root);

        if (countAnswers % 2 == 0) {
            for (int i = 8; i > countAnswers; i--) {
                constraintSet.setVerticalWeight(getResources().getIdentifier("btn" + i, "id", getActivity().getPackageName()), 0);
            }
            constraintSet.applyTo(root);
        } else {
            countAnswers++;
            for (int i = 8; i > countAnswers; i--) {
                constraintSet.setVerticalWeight(getResources().getIdentifier("btn" + i, "id", getActivity().getPackageName()), 0);
            }
            constraintSet.applyTo(root);
            View btn = v.findViewById(getResources().getIdentifier("btn" + countAnswers, "id", getActivity().getPackageName()));
            btn.setVisibility(View.INVISIBLE);//btn.setEnabled(false);

        }


        ConstraintSet constraintSet2 = new ConstraintSet();
        constraintSet2.clone((ConstraintLayout) v.findViewById(R.id.fragment_constraint_layout));


        constraintSet2.setGuidelinePercent(R.id.fragment_guidline,
                (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        (1 - getResources().getFraction(R.fraction.fragment_answers_btn_height, 1, 1) * countAnswers) : 0.6f);

        constraintSet2.applyTo((ConstraintLayout) v.findViewById(R.id.fragment_constraint_layout));

    }

    public void setHalfCountAnswers(List<Integer> list, String answer) {


        boolean value = true;
        for (int i = 0; i < list.size(); i++) {

            Button btn = (Button) getView()
                    .findViewById(getResources()
                            .getIdentifier("btn" + list.get(i), "id", getActivity().getPackageName()));


            if (answer.equals(btn.getText())) {
                value = false;
                continue;
            }

            btn.setEnabled(false);
            if (value && i == (list.size() - 2)) break;

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (Callback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }


    /*@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG", "onActivityCreated call " + pageNumber);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TAG", "onStart call " + pageNumber);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAG", "onResume call " + pageNumber);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG", "onPause call " + pageNumber);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG", "onStop call " + pageNumber);
    }*/
}
