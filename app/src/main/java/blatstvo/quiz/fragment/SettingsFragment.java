package blatstvo.quiz.fragment;

import android.os.Bundle;

import android.preference.PreferenceFragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import android.support.v7.preference.SeekBarPreference;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import blatstvo.quiz.R;


public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        SeekBarPreference sb = ((SeekBarPreference) findPreference("seekbar_preference_key"));
        sb.setMin(4);
    }



}